import glob
import scipy.misc
import cv2
import os
import numpy as np
from transform import four_point_transform

images = [f for f in glob.glob("test-images/*.JPG")]

for img in images:
	
	prefix = os.path.basename(img.split('.')[0])
	
	image = cv2.imread(img)
	ratio = 0.5
	image = scipy.misc.imresize(image, ratio)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (5, 5), 0)
	edged = cv2.Canny(gray, 30, 100)
	
	cv2.imwrite(prefix+'edged.png',edged)

	#restore edges
	kernel = np.ones((5,5),np.uint8)
	edged = cv2.dilate(edged,kernel,iterations =4)
	edged = cv2.erode(edged,kernel,iterations =4)
		
	cv2.imwrite(prefix+'edged2.png',edged)
	cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	cnts = sorted(cnts[1], key = cv2.contourArea, reverse = True)

	for c in cnts:
		
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.02 * peri, True)


		if len(approx) == 4:
			screenCnt = approx
			break
		
	image2 = image.copy()
	cv2.drawContours(image2, [screenCnt], -1, (0, 255, 0), 2)
	cv2.imwrite(prefix+'edged3.png',image2)

	warped = four_point_transform(image, screenCnt.reshape(4, 2)) 

	
	cv2.imwrite(prefix+'warped.png',warped)
